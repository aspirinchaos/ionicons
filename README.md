# Ionic icons

Blaze-Компонент для [ionicons](https://ionicons.com/).

Реализует простую работу с отображением иконок для ionicons.

### Использование компонента

#### Вызов компонета в тимплейте
```spacebars
{{> IonIcon name='add-circle-outline'}}
```

#### Вызов компонента в контроллере
```js
import { IonIcon } from 'meteor/ionicons'
// возвращает HTML 
IonIcon({name: 'paperclip'});

```
### Параметры
```javascript
const props = {
  // дополнительный класс
  className: { type: String, optional: true },
  // имя иконки
  name: String,
  // тип иконки, по умолчанию ios
  type: { type: String, defaultValue: 'ios', allowedValues: ['ios', 'md', 'logo'] },
}
``` 

### TODO

- Тестирование компонента

