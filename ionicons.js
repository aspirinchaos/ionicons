import { TemplateController } from 'meteor/template-controller';
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import SimpleSchema from 'simpl-schema';
import c from 'classnames';

import './ionicons.html';

checkNpmVersions({ 'simpl-schema': '1.5.3', classnames: '2.2.6' }, 'ionicons');

const IonIcon = TemplateController('IonIcon', {
  props: new SimpleSchema({
    className: { type: String, optional: true },
    type: { type: String, defaultValue: 'ios', allowedValues: ['ios', 'md', 'logo'] },
    name: String,
  }),
  helpers: {
    className() {
      const {
        name, type, className,
      } = this.props;
      return c({
        icon: true,
        [`ion-${type}-${name}`]: true,
        [className]: !!className,
      });
    },
  },
});

export { IonIcon };
