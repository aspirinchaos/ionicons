Package.describe({
  name: 'ionicons',
  version: '0.1.1',
  summary: 'Ionicons blaze component',
  git: 'https://bitbucket.org/aspirinchaos/ionicons.git',
  documentation: 'README.md',
});

Npm.depends({
  ionicons: '4.2.4',
});

Package.onUse((api) => {
  api.versionsFrom('1.7');
  api.use([
    'ecmascript',
    'tmeasday:check-npm-versions',
    'fourseven:scss',
    'templating',
    'template-controller',
  ]);
  api.addAssets([
    'fonts/ionicons.eot',
    'fonts/ionicons.woff2',
    'fonts/ionicons.woff',
    'fonts/ionicons.ttf',
    'fonts/ionicons.svg',
  ], 'client');
  api.addFiles('ionicons.scss', 'client');
  api.mainModule('ionicons.js', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('ionicons');
  api.mainModule('ionicons-tests.js');
});
